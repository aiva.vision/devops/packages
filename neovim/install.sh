#!/bin/bash

package_name="neovim"
tag="master"

optstring=":ht:DC"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -t      tag\n"
  printf "            default: ${tag}\n"
  printf "    -D      install dependencies\n"
  printf "    -C      clean\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    t)
      tag=${OPTARG}
      ;;
    D)
      sudo apt install -y \
        ninja-build \
        gettext \
        libtool \
        libtool-bin \
        autoconf \
        automake \
        g++ \
        pkg-config \
        unzip \
        curl \
        doxygen
      ;;
    C)
      should_clean="yes"
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d "${package_name}" ]]; then

  echo "INFO: ${package_name} is already cloned."
  git -C "${package_name}" pull

else
  git clone https://github.com/neovim/neovim.git
fi

git -C "${package_name}" checkout "$tag"

if [[ -v should_clean ]]; then
  sudo make distclean -C "${package_name}"
fi

make CMAKE_BUILD_TYPE=RelWithDebInfo -j$(nproc) -C "${package_name}"

sudo make install -C "${package_name}"
