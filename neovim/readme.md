# NeoVim build script

This script builds [NeoVim](https://github.com/neovim/neovim) from source.

### Usage:

```zsh
Usage: install.sh [:ht:DC]

    -h      print this
    -t      tag
            default: stable
    -D      install dependencies
    -C      clean
```
