# nlohman_json build script

This script builds [nlohman_json](https://github.com/nlohmann/json) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
