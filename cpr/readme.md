# cpr build script

This script builds [cpr](https://github.com/libcpr/cpr) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
