#!/bin/bash

package_name="gstreamer"
version="main"

optstring=":hv:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
  printf "    -D      install dependencies\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    D)
      install_dependencies=1
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -v install_dependencies ]]; then
  pip3 install meson
  sudo pip3 install meson
  sudo apt install -y flex ninja-build bison
fi

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout main
  git -C "${package_name}" pull
else
  git clone https://gitlab.freedesktop.org/gstreamer/gstreamer.git
fi

git -C "${package_name}" checkout "$version"

original_directory=$(pwd)

cd gstreamer
meson --prefix ${PACKAGES_PREFIX:-:"/usr/local"} build
ninja -j $(expr $(nproc) - 1) -C build
meson install -C build
status=$?

cd ${original_directory}
exit ${status}
