# GStreamer

**WARNING**: overriding the GStreamer libraries shipped with the OS may break the OS.

These scripts can be used for building GStreamer from source and base Docker images.

### Bare metal

This script will install GStreamer to `/opt/gstreamer`.

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
    -D      install dependencies
```

Don't forget to configure the environment for the custom GStreamer location.

```zsh
export LD_LIBRARY_PATH="/opt/gstreamer/lib/aarch64-linux-gnu"${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export PKG_CONFIG_PATH="/opt/gstreamer/lib/aarch64-linux-gnu/pkgconfig"${PKG_CONFIG_PATH:+:${PKG_CONFIG_PATH}}
```

### Docker

```bash
./build_image.sh
Usage: build_image.sh [:hv:g:u:a:]

   -h      print this
   -v      cmake_version
   -g      gcc_version
   -u      ubuntu_version, codename or release
   -a      architecture, arm64v8 or amd64
```
