# opentelemetry-cpp build script

This script builds [opentelemetry-cpp](https://github.com/open-telemetry/opentelemetry-cpp) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
