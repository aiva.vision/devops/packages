#!/bin/bash

package_name="opentelemetry-cpp"
version="master"

optstring=":hv:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d ${package_name} ]]; then
  echo ${package_name}
  git -C ${package_name} pull
else
  git clone https://github.com/open-telemetry/opentelemetry-cpp.git
fi

git -C ${package_name} checkout "$version"
git -C ${package_name} submodule update --init --recursive

mkdir -p ${package_name}/release
cmake -S ${package_name} -B ${package_name}/release \
  -D CMAKE_INSTALL_PREFIX=${PACKAGES_PREFIX:-:"/usr/local"} \
  -D CMAKE_BUILD_TYPE=Release \
  -D WITH_EXAMPLES=NO \
  -D BUILD_TESTING=NO
cmake --build ${package_name}/release -j

if [[ $EUID -ne 0 ]]; then
  sudo cmake --install ${package_name}/release
else
  cmake --install ${package_name}/release
fi
