# spdlog build script

This script builds [spdlog](https://github.com/gabime/spdlog) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: v1.x
```
