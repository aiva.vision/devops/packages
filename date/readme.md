# date build script

This script builds [date](https://github.com/HowardHinnant/date) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
