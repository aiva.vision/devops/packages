# yaml-cpp build script

This script builds [yaml-cpp](https://github.com/jbeder/yaml-cpp) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
