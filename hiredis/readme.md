# hiredis build script

This script builds [hiredis](https://github.com/redis/hiredis/) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
