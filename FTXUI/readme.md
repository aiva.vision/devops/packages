# FTXUI build script

This script builds [FTXUI](https://github.com/ArthurSonzogni/ftxui) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
