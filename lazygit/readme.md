# lazygit install script

This script downloads and installs [lazygit](https://github.com/jesseduffield/lazygit).

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: latest
```
