#!/bin/bash

package_name="librdkafka"
version="master"

optstring=":hv:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
  printf "    -D      install dependencies\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    D)
      sudo apt-get install -y libsasl2-dev liblz4-dev
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done


if [ -z ${version} ]; then
  echo "$(basename $0): Setting the version is mandatory."
  exit 1
fi

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone https://github.com/edenhill/librdkafka.git
fi

git -C "${package_name}" checkout "$version"

original_directory=$(pwd)
cd "${package_name}"
./configure --prefix=${PACKAGES_PREFIX:-:"/usr/local"}
cd ${original_directory}

make -j $(nproc) -C "${package_name}"
sudo make install -C "$package_name"
