# librdkafka build script

This script builds [librdkafka](https://github.com/edenhill/librdkafka) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
