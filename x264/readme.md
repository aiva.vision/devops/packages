# x264

These scripts can be used for building x264 from source.

### Bare metal

```zsh
./install.sh -h
Usage: install.sh [:hv:D]

    -h      print this
    -v      version
    -D      install dependencies
```
