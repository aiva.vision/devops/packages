#!/bin/bash

repo=https://code.videolan.org/videolan/x264.git
package_name="x264"
version="stable"

optstring=":hv:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
  printf "    -D      install dependencies\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    D)
      sudo apt-get install -y nasm
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone ${repo} ${package_name}
fi

git -C "${package_name}" checkout "$version"

original_directory=$(pwd)
cd ${package_name}

./configure --prefix=${PACKAGES_PREFIX:-:"/usr/local"} \
    --enable-static \
    --enable-shared \
    --enable-pic \
  && make -j $(($(nproc)-2)) \
  && sudo make install
status=$?

cd ${original_directory}
exit ${status}
