# clipp build script

This script builds [clipp](https://github.com/muellan/clipp.git) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
