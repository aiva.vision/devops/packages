#!/bin/bash

package_name="leftwm-theme"
tag="master"

optstring=":ht:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -t      tag\n"
  printf "            default: ${tag}\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    t)
      tag=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d "${package_name}" ]]; then

  echo "INFO: ${package_name} is already cloned."
  git -C "${package_name}" pull

else
  git clone https://github.com/leftwm/${package_name}.git
fi

git -C "${package_name}" checkout "$tag"

cd ${package_name}

cargo build --release
sudo cp target/release/${package_name} /usr/local/bin

cd ..
