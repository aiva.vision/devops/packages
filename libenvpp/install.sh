#!/bin/bash

package_name="libenvpp"
version="v1.3.0"

optstring=":hv:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d ${package_name} ]]; then
  echo ${package_name}
  git -C ${package_name} pull
else
  git clone https://github.com/ph3at/libenvpp.git
fi

git -C ${package_name} checkout "$version"

mkdir -p ${package_name}/release
cmake -S ${package_name} -B ${package_name}/release \
  -D CMAKE_INSTALL_PREFIX=${PACKAGES_PREFIX:-:"/usr/local"} \
  -D BUILD_SHARED_LIBS=ON \
  -D LIBENVPP_INSTALL=ON \
  -D LIBENVPP_TESTS=OFF \
  -D LIBENVPP_EXAMPLES=OFF \
  -D CMAKE_BUILD_TYPE=Release
cmake --build ${package_name}/release -j

if [[ $EUID -ne 0 ]]; then
  sudo cmake --install ${package_name}/release
else
  cmake --install ${package_name}/release
fi
