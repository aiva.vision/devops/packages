# libenvpp build script

This script builds [libenvpp](https://github.com/ph3at/libenvpp) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
