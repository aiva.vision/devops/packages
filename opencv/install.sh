#!/bin/bash

package_name="opencv"
compute_capabilities="6.1,7.5,8.6"
cores=$(($(nproc)-2))

optstring=":hv:j:C:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "    -j      cores count\n"
  printf "            default: ${cores}\n"
  printf "    -C      compute_capabilities\n"
  printf "            default: ${compute_capabilities}\n"
  printf "    -D      install dependencies\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    j)
      cores=${OPTARG}
      ;;
    C)
      compute_capabilities=${OPTARG}
      ;;
    D)
      install_dependencies=1
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

variables=("version")

for variable_name in ${variables[@]}
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done

cudnn_header=/usr/include/cudnn_version.h

if [[ ! -f "${cudnn_header}" ]]; then
  echo "$(basename $0): CUDNN header could not be found at ${cudnn_header}."
  exit 1
fi

major=$(grep "CUDNN_MAJOR" ${cudnn_header} | head -n 1  | cut -d " " -f 3)
minor=$(grep "CUDNN_MINOR" ${cudnn_header} | head -n 1 | cut -d " " -f 3)
patch=$(grep "CUDNN_PATCHLEVEL" ${cudnn_header} | head -n 1 | cut -d " " -f 3)

if [[ -z "${major}" || -z "${minor}" || -z "${patch}" ]]; then
  echo "$(basename $0): Failed to parse CUDNN version from ${cudnn_header}."
  exit 1
fi

cudnn_version="${major}.${minor}.${patch}"

echo "$(basename $0): Building for CUDNN ${cudnn_version}."

if [[ -v install_dependencies ]]; then
  # open-cv has a lot of dependencies, but most can be found in the default
  # package repository or should already be installed (eg. CUDA).
  echo "Installing build dependencies."
  sudo apt update
  sudo apt install -y \
    build-essential \
    git \
    gfortran \
    libatlas-base-dev \
    libavcodec-dev \
    libavformat-dev \
    libcanberra-gtk3-module \
    libdc1394-dev \
    libeigen3-dev \
    libglew-dev \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer-plugins-good1.0-dev \
    libgstreamer1.0-dev \
    libgtk-3-dev \
    libjpeg-dev \
    libjpeg8-dev \
    libjpeg-turbo8-dev \
    liblapack-dev \
    liblapacke-dev \
    libopenblas-dev \
    libpng-dev \
    libpostproc-dev \
    libswscale-dev \
    libtbb-dev \
    libtbb2 \
    libtesseract-dev \
    libtiff-dev \
    libv4l-dev \
    libxine2-dev \
    libxvidcore-dev \
    libx264-dev \
    pkg-config \
    python3-dev \
    python3-numpy \
    python3-matplotlib \
    qv4l2 \
    v4l-utils \
    zlib1g-dev
fi

packages=("${package_name}" "${package_name}_contrib")

for package in "${packages[@]}"
do
  if [[ -d "${package}" ]]; then
    echo "${package} is already present."
    git -C "${package}" checkout master
    git -C "${package}" pull
  else
    git clone https://github.com/opencv/${package}.git
  fi

  git -C "${package}" checkout "$version"
done

mkdir -p ${package_name}/release

cmake -S ${package_name} -B ${package_name}/release \
  -D CMAKE_INSTALL_PREFIX=${PACKAGES_PREFIX:-:"/usr/local"} \
  -D CMAKE_EXPORT_COMPILE_COMMANDS=1 \
  -D CMAKE_BUILD_TYPE=Release \
  -D BUILD_opencv_python2=OFF \
  -D BUILD_opencv_python3=ON \
  -D CUDA_ARCH_BIN=${compute_capabilities} \
  -D CUDA_FAST_MATH=ON \
  -D CUDNN_VERSION=${cudnn_version} \
  -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
  -D OPENCV_DNN_CUDA=ON \
  -D OPENCV_ENABLE_NONFREE=ON \
  -D OPENCV_EXTRA_MODULES_PATH=opencv_contrib/modules \
  -D OPENCV_GENERATE_PKGCONFIG=ON \
  -D WITH_CUBLAS=ON \
  -D WITH_CUDA=ON \
  -D WITH_CUDNN=ON \
  -D WITH_GSTREAMER=ON \
  -D WITH_FFMPEG=ON \
  -D WITH_LIBV4L=ON \
  -D WITH_OPENGL=ON \
  -D BUILD_EXAMPLES=OFF \
  -D BUILD_PERF_TESTS=OFF \
  -D BUILD_TESTS=OFF

cmake --build ${package_name}/release -j ${cores}

if [[ $EUID -ne 0 ]]; then
  sudo cmake --install ${package_name}/release
else
  cmake --install ${package_name}/release
fi
