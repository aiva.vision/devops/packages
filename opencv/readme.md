# OpenCV build script

This script builds and installs OpenCV from source with CUDA support.

### Usage

Check [here](https://developer.nvidia.com/cuda-gpus) to find your GPU's compute capability.

```zsh
Usage: install.sh [:hv:j:C:D]

    -h      print this
    -v      version
    -j      cores count
            default: 10
    -C      compute_capabilities
            default: 6.1,7.5,8.6
    -D      install dependencies
```
