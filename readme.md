# Third party packages

This repository contains scripts for building from source or for building Docker images.

All the libraries will be installed under `PACKAGES_PREFIX` if set, or under `/usr/local` by default. Configure your environment as needed.

```sh
export PACKAGES_PREFIX="/opt/packages"
export PATH=${PACKAGES_PREFIX}/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=${PACKAGES_PREFIX}/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export PKG_CONFIG_PATH=${PACKAGES_PREFIX}/lib/pkgconfig${PKG_CONFIG_PATH:+:${PKG_CONFIG_PATH}}
export CMAKE_PREFIX_PATH=${PACKAGES_PREFIX}${CMAKE_PREFIX_PATH:+:${CMAKE_PREFIX_PATH}}
```
