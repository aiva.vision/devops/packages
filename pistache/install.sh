#!/bin/bash

package_name="pistache"
version="master"

optstring=":hv:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d ${package_name} ]]; then
  echo ${package_name}
  git -C ${package_name} pull
else
  git clone https://github.com/pistacheio/pistache.git
fi

git -C ${package_name} checkout "$version"

cd ${package_name}

meson setup --prefix ${PACKAGES_PREFIX:-:"/usr/local"} build
meson install -C build
status=$?

cd ..
exit ${status}
