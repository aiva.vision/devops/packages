# DPP build script

This script builds [DPP](https://github.com/brainboxdotcc/DPP) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
