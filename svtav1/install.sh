#!/bin/bash

repo=https://gitlab.com/AOMediaCodec/SVT-AV1.git
package_name="svtav1"
version="v1.7.0"

optstring=":hv:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone ${repo} ${package_name}
fi

git -C "${package_name}" checkout "$version"

mkdir -p ${package_name}/release
cmake -S ${package_name} -B ${package_name}/release \
  -D CMAKE_INSTALL_PREFIX=${PACKAGES_PREFIX:-:"/usr/local"} \
  -D CMAKE_POSITION_INDEPENDENT_CODE=ON \
  -D CMAKE_BUILD_TYPE=Release \
  -D BUILD_DEC=OFF
cmake --build ${package_name}/release -j $(($(nproc)-2))

if [[ $EUID -ne 0 ]]; then
  sudo cmake --install ${package_name}/release
else
  cmake --install ${package_name}/release
fi
