# svtav1

These scripts can be used for building svtav1 from source.

### Bare metal

```zsh
./install.sh -h
Usage: install.sh [:hv:D]

    -h      print this
    -v      version
    -D      install dependencies
```
