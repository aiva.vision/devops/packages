#!/bin/bash

package_name="FFmpeg"
version="n5.1.3"

optstring=":hv:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
  printf "    -D      install dependencies\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    D)
      install_dependencies=1
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -v install_dependencies ]]; then
  sudo apt update && sudo apt install -y \
    nasm \
    libx264-dev \
    libx265-dev libnuma-dev \
    libvpx-dev \
    libfdk-aac-dev \
    libopus-dev \
    libgnutls28-dev \
    libunistring-dev \
    libaom-dev \
    libass-dev \
    libdav1d-dev \
    libmp3lame-dev \
    libsvtav1enc-dev \
    libvorbis-dev \
    libsdl2-dev
fi

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone https://github.com/FFmpeg/FFmpeg
fi

git -C "${package_name}" checkout "$version"

original_directory=$(pwd)
cd ${package_name}

  # --pkg-config-flags="--static" \
./configure \
  --prefix="${PACKAGES_PREFIX:-:"/usr/local"}" \
  --extra-libs="-lpthread -lm" \
  --ld="g++" \
  --enable-pic \
  --enable-shared \
  --enable-gpl \
  --enable-gnutls \
  --enable-libaom \
  --enable-libass \
  --enable-libfdk-aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libsvtav1 \
  --enable-libdav1d \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 \
  --enable-nonfree && \
sudo make install -j $(($(nproc)-2)) && \
hash -r
status=$?

cd ${original_directory}
exit ${status}
