# FFmpeg

These scripts can be used for building FFmpeg from source.

### Bare metal

```zsh
./install.sh -h
Usage: install.sh [:hv:D]

    -h      print this
    -v      version
            default: n5.1.3
    -D      install dependencies
```
