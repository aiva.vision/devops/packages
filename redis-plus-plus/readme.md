# redis-plus-plus build script

This script builds [redis-plus-plus](https://github.com/sewenew/redis-plus-plus) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
