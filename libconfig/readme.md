# libconfig build script

This script builds [libconfig](https://github.com/hyperrealm/libconfig) from source.

### Usage:

```zsh
./build.sh -h
Usage: build.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
