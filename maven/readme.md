# Maven

These scripts installs Maven on the host.

Check the latest Maven version [here](https://maven.apache.org/download.cgi).

### Usage:

```zsh
./install.sh
Usage: install.sh [:hv:]

    -h      print this
    -v      version
```
