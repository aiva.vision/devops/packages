#!/bin/bash

optstring=":hv:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

variables=("version")

for variable_name in ${variables[@]}
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done

wget https://downloads.apache.org/maven/maven-3/${version}/binaries/apache-maven-${version}-bin.tar.gz

# extract the binaries
tar -xzvf apache-maven-${version}-bin.tar.gz
sudo mv apache-maven-${version} /opt/apachemaven

# configure the environment
cat <<"EOF" >> ${ZDOTDIR+${ZDOTDIR}/}.zshenv
export JAVA_HOME=/usr/lib/jvm/default-java
export M2_HOME=/opt/apachemaven
export MAVEN_HOME=/opt/apachemaven
export PATH=${M2_HOME}/bin:${PATH}
EOF
