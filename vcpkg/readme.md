# vcpkg build script

This script builds [vcpkg](https://github.com/microsoft/vcpkg) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:D]

    -h      print this
    -v      version
            default: master
    -D      install dependencies
```
