#!/bin/bash

package_name="vcpkg"
version="master"

optstring=":hv:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
  printf "    -D      install dependecies\n"
}
while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    D)
      sudo apt update && sudo apt install -y curl zip unzip tar ninja-build
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d ${package_name} ]]; then
  echo ${package_name}
  git -C ${package_name} pull
else
  git clone https://github.com/microsoft/vcpkg.git
fi

git -C ${package_name} checkout "$version"

./vcpkg/bootstrap-vcpkg.sh
sudo cp ./vcpkg/vcpkg /usr/local/bin
