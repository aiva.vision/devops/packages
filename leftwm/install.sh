#!/bin/bash

package_name="leftwm"
tag="master"

optstring=":ht:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -t      tag\n"
  printf "            default: ${tag}\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    t)
      tag=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d "${package_name}" ]]; then

  echo "INFO: ${package_name} is already cloned."
  git -C "${package_name}" pull

else
  git clone https://github.com/leftwm/${package_name}.git
fi

git -C "${package_name}" checkout "$tag"

cd ${package_name}

cargo build --profile optimized

sudo cp target/optimized/leftwm /usr/bin/leftwm
sudo cp target/optimized/leftwm-worker /usr/bin/leftwm-worker
sudo cp target/optimized/lefthk-worker /usr/bin/lefthk-worker
sudo cp target/optimized/leftwm-state /usr/bin/leftwm-state
sudo cp target/optimized/leftwm-check /usr/bin/leftwm-check
sudo cp target/optimized/leftwm-command /usr/bin/leftwm-command

sudo cp leftwm.desktop /usr/share/xsessions/

cd ..
