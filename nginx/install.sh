#!/bin/bash

package_name="nginx"

optstring=":hv:dc:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "    -d      install dependencies\n"
  printf "    -c      config options\n"
  printf "            wrap with \"\" for multiple options\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    d)
      sudo apt update && sudo apt install -y gcc make libpcre3-dev zlib1g-dev libssl-dev
      ;;
    c)
      config_options=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [ -z "${version}" ]; then
  echo "$(basename $0): Setting the version is mandatory."
  exit 1
fi

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone https://github.com/nginx/nginx.git
fi

git -C "${package_name}" checkout release-"$version"

original_directory=$(pwd)
cd ${package_name}

./auto/configure \
  --sbin-path=/usr/local/bin/nginx \
  --conf-path=/etc/nginx/nginx.conf \
  --error-log-path=/var/log/nginx/error.log \
  --http-log-path=/var/log/nginx/access.log \
  --pid-path=/run/nginx.pid \
  --with-http_ssl_module \
  --with-http_v2_module ${config_options}

cd ${original_directory}

sudo make install -j $(nproc) -C "${package_name}"

# copy service file
sudo cp systemd/nginx.service /etc/systemd/system

# enable and start nginx
sudo systemctl enable --now nginx
