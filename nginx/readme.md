# Nginx build script

This script installs [Nginx](https://nginx.org).

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:dc:]

    -h      print this
    -v      version
    -d      install dependencies
    -c      config options
            wrap with "" for multiple options
```
