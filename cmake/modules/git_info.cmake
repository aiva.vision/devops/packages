find_package(Git REQUIRED)

execute_process(
  COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
  RESULT_VARIABLE result
  OUTPUT_VARIABLE PROJECT_GIT_HASH
  OUTPUT_STRIP_TRAILING_WHITESPACE)

if(result)
  message(FATAL_ERROR "Failed to get git hash: ${result}")
endif()

execute_process(
  COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
  RESULT_VARIABLE result
  OUTPUT_VARIABLE PROJECT_GIT_SHORT_HASH
  OUTPUT_STRIP_TRAILING_WHITESPACE)

if(result)
  message(FATAL_ERROR "Failed to get git hash: ${result}")
endif()
