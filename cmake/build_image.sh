#!/bin/bash

optstring=":hv:g:u:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      cmake_version\n"
  printf "    -g      gcc_version\n"
  printf "    -u      ubuntu_version, codename or release\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      cmake_version=${OPTARG}
      ;;
    g)
      gcc_version=${OPTARG}
      ;;
    u)
      ubuntu_version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

for variable_name in "cmake_version" "gcc_version" "ubuntu_version"
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done



name="registry.gitlab.com/aiva.vision/devops/packages/cmake"
tag_suffix="gcc${gcc_version}-${ubuntu_version}"
tag="${cmake_version}-${tag_suffix}"
major_tag="${cmake_version:0:4}-${tag_suffix}"
latest_tag="latest-${tag_suffix}"

echo "Building image with tag ${tag} ..."
docker buildx build \
  --push \
  --platform linux/amd64,linux/arm64 \
  --cpuset-cpus $(nproc) \
  --build-arg cmake_version=${cmake_version} \
  --build-arg gcc_version=${gcc_version} \
  --build-arg ubuntu_version=${ubuntu_version} \
  -t ${name}:${tag} \
  -t ${name}:${major_tag} \
  -t ${name}:${latest_tag} \
  .
