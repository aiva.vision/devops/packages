#!/bin/bash

package_name="CMake"
version="master"

optstring=":hv:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
  printf "    -D      install dependencies\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    D)
      install_dependencies=1
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -v install_dependencies ]]; then
  sudo apt update && sudo apt install -y \
    libcurl4-openssl-dev \
    libssl-dev \
    libncurses-dev \
    make \
    wget
fi

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone https://github.com/Kitware/CMake.git
fi

git -C "${package_name}" checkout "v$version"

original_directory=$(pwd)

cd CMake
./bootstrap --prefix=${PACKAGES_PREFIX:-:"/usr/local"}
make -j$(($(nproc)-1))
sudo make install
status=$?

cd ${original_directory}
exit ${status}
