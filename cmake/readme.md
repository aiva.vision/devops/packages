# CMake

These scripts can be used for building CMake from source and base Docker images.

### Bare metal

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
    -D      install dependencies
```

### Docker

These images serve as bases for other containers. Check the latest available version [here](https://cmake.org/download/).

Preferably, development images should be built on top of this one, but CMake can be extracted from the following locations using `COPY --from=<cmake_tag>`:

- `/usr/bin/cmake`
- `/usr/local/share/cmake-<major>.<minor>`
- `/usr/local/doc/cmake-<major>.<minor>`

```bash
./build_image.sh
Usage: build_image.sh [:hv:g:u:a:]

   -h      print this
   -v      cmake_version
   -g      gcc_version
   -u      ubuntu_version, codename or release
   -a      architecture, arm64v8 or amd64
```
