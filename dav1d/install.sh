#!/bin/bash

package_name="dav1d"
version="1.3.0"

optstring=":hv:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
  printf "    -D      install dependencies\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    D)
      install_dependencies=1
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -v install_dependencies ]]; then
  sudo apt update && sudo apt install -y meson
fi

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone https://github.com/videolan/dav1d.git
fi

git -C "${package_name}" checkout "$version"

mkdir -p ${package_name}/build

original_directory=$(pwd)
cd ${package_name}/build

meson setup \
  --prefix=${PACKAGES_PREFIX:-:"/usr/local"}  \
  --libdir=${PACKAGES_PREFIX:-:"/usr/local"}/lib \
  -Db_staticpic=true \
  -Denable_tests=false \
  --default-library=static .. \
  && ninja -j $(($(nproc)-2)) \
  && sudo ninja install
status=$?

cd ${original_directory}
exit ${status}
