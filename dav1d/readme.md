# dav1d

These scripts can be used for building dav1d from source.

### Bare metal

```zsh
./install.sh -h
Usage: install.sh [:hv:D]

    -h      print this
    -v      version
    -D      install dependencies
```
