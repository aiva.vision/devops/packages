# trompeloeil build script

This script builds [trompeloeil](https://github.com/rollbear/trompeloeil.git) from source and installs it to the default location.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
