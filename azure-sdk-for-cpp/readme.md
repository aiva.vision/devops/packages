# azure-sdk-for-cpp build script

This script builds [azure-sdk-for-cpp](https://github.com/Azure/azure-sdk-for-cpp.git) from source.

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: main
```
