#!/bin/bash

package_name="picocom"
version="master"

optstring=":hv:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "            default: ${version}\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d "${package_name}" ]]; then
  echo "${package_name} is already present."
  git -C "${package_name}" checkout master
  git -C "${package_name}" pull
else
  git clone https://github.com/npat-efault/picocom.git
fi

git -C "${package_name}" checkout "$version"

make -j $(nproc) -C "${package_name}"
sudo cp ${package_name}/${package_name} /usr/local/bin

mkdir -p ~/.local/man/man1
cp ${package_name}/${package_name}.1 ~/.local/man/man1
