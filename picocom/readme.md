# picocom build script

This script installs [picocom](https://github.com/npat-efault/picocom).

### Usage:

```zsh
./install.sh -h
Usage: install.sh [:hv:]

    -h      print this
    -v      version
            default: master
```
