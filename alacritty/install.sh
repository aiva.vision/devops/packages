#!/bin/bash

package_name="alacritty"
tag="master"

optstring=":ht:D"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -t      tag\n"
  printf "            default: ${tag}\n"
  printf "    -D      install dependencies\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    t)
      tag=${OPTARG}
      ;;
    D)
      curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
      rustup override set stable
      rustup update stable
      sudo apt install -y \
	      pkg-config \
	      libfreetype6-dev \
	      libfontconfig1-dev \
	      libxcb-xfixes0-dev \
	      libxkbcommon-dev \
	      python3
      source "$HOME/.cargo/env"
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [[ -d "${package_name}" ]]; then

  echo "INFO: ${package_name} is already cloned."
  git -C "${package_name}" pull

else
  git clone https://github.com/${package_name}/${package_name}.git
fi

git -C "${package_name}" checkout "$tag"

cd ${package_name}

cargo build --release
sudo cp target/release/alacritty /usr/local/bin

sudo tic -xe alacritty,alacritty-direct extra/alacritty.info
sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
sudo desktop-file-install extra/linux/Alacritty.desktop
sudo update-desktop-database

cd ..
