# Alacritty build script

This script builds [Alacritty](https://github.com/alacritty/alacritty) from source.

### Usage:

```zsh
Usage: install.sh [:ht:D]

    -h      print this
    -t      tag
            default: master
    -D      install dependencies
```
