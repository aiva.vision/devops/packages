#!/bin/bash

if [[ ! -f "geoserver.zip" ]]; then
  echo "Download and rename the GeoServer archive first."
  exit 1
fi

# add the geoserver user
sudo adduser \
  --system \
  --shell /bin/false \
  --group \
  --disabled-password \
  geoserver

# install Kafka
sudo mkdir -p /opt/geoserver
sudo unzip geoserver.zip -d /opt/geoserver
sudo chown -R geoserver:geoserver /opt/geoserver
# copy the service file
sudo cp systemd/* /etc/systemd/system/
# copy configs
sudo cp rsyslog/* /etc/rsyslog.d/
sudo cp logrotate/* /etc/logrotate.d/
# create the log files
sudo mkdir -p /var/log/geoserver
sudo touch /var/log/geoserver/geoserver.log
# set permissions
sudo chown -R syslog:adm /var/log/geoserver
# apply changes
sudo systemctl restart rsyslog

# configure the environment
cat <<"EOF" | sudo tee -a /etc/zsh/zshenv

# Kafka
export GEOSERVER_HOME="/opt/geoserver"
EOF

exit 0
