# GeoServer

1. Download the latest binary from [here](http://geoserver.org/release/stable/).

2. Rename the zip to `geoserver.zip`.

   ```zsh
   mv <file-name>.zip geoserver.zip
   ```

3. Install dependencies.

   ```zsh
   sudo apt install -y openjdk-11-jdk unzip
   ```

4. Install.

   ```zsh
   ./install.sh
   ```
