#!/bin/bash

if [[ ! -d kafka ]]; then
  echo "Download Kafka first using download_kafka.sh"
  exit 1
fi

# add kafka user
sudo adduser \
  --system \
  --shell /bin/false \
  --group \
  --disabled-password \
  kafka

# install Kafka
sudo cp -r kafka /opt/kafka
sudo chown -R kafka:kafka /opt/kafka
# copy the service file
sudo cp systemd/* /etc/systemd/system/
# copy configs
sudo cp rsyslog/* /etc/rsyslog.d/
sudo cp logrotate/* /etc/logrotate.d/
# create the log files
sudo mkdir -p /var/log/kafka
sudo touch /var/log/kafka/{kafka.log,zookeeper.log}
# set permissions
sudo chown -R syslog:adm /var/log/kafka
# apply changes
sudo systemctl restart rsyslog

# configure the environment
cat <<"EOF" >> ${ZDOTDIR+${ZDOTDIR}/}.zshenv

# Kafka
export KAFKA_HOME="/opt/kafka"
export PATH=${PATH}:${KAFKA_HOME}/bin
export BOOTSTRAP_SERVERS=localhost:9092
EOF

exit 0
