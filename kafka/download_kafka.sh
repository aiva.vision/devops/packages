#!/bin/bash

optstring=":hv:s:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      kafka_version\n"
  printf "    -s      scala_version\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      kafka_version=${OPTARG}
      ;;
    s)
      scala_version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

for variable_name in "kafka_version" "scala_version"
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done

file_name="kafka_${scala_version}-${kafka_version}.tgz"
url="https://downloads.apache.org/kafka/${kafka_version}/kafka_${scala_version}-${kafka_version}.tgz"

if [[ -f "${file_name}" ]]; then
  echo "The binaries for Kafka ${kafka_version} and Scala ${scala_version} are already present."
else
  echo "Downloading Kafka from $url ..."
  wget "${url}"
fi

tar -xzf "${file_name}"
mv "kafka_${scala_version}-${kafka_version}" kafka
