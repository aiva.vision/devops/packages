# Kafka

This section is derived from wurstmeister's [kafka-docker](https://github.com/wurstmeister/kafka-docker) repository.

# Download

```zsh
./download_kafka.sh
Usage: download_kafka.sh [:hv:s:]

    -h      print this
    -v      kafka_version
    -s      scala_version
```

# Install

```zsh
./install.sh
```
