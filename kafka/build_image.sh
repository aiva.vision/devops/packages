#!/bin/bash

optstring=":hv:s:j:d:a:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      kafka_version\n"
  printf "    -s      scala_version\n"
  printf "    -j      java_version\n"
  printf "    -d      debian_version\n"
  printf "    -a      architecture\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      kafka_version=${OPTARG}
      ;;
    s)
      scala_version=${OPTARG}
      ;;
    j)
      java_version=${OPTARG}
      ;;
    d)
      debian_version=${OPTARG}
      ;;
    a)
      architecture=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

variables=("kafka_version" "scala_version" "java_version" "debian_version" "architecture")

for variable_name in "${variables[@]}"
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done

read -r -d '' docker_build <<"EOF"
DOCKER_BUILDKIT=1 docker build \
  --cpuset-cpus $(nproc) \
  --build-arg kafka_version=${kafka_version} \
  --build-arg scala_version=${scala_version} \
  --build-arg java_version=${java_version} \
  --build-arg debian_version=${debian_version} \
  --build-arg architecture=${architecture} \
  -t ${name}:${tag} .
EOF

name="registry.gitlab.com/aiva.vision/devops/packages/kafka"
tag="${kafka_version}-s${scala_version}-jre${java_version}-${debian_version}-${architecture}"

echo "Building image with tag ${tag} ..."
echo "${docker_build}"
eval "${docker_build}"

docker push ${name}:${tag}
